# Daczo Alpar - daim1803
from io import TextIOWrapper
from typing import Any
import numpy as np
import pathlib

def get_line(input_file: TextIOWrapper) -> list[str]:
    return input_file.readline().splitlines()[0].split(' ')

class Graph:

    def __init__(self) -> None:
        self.nodes = []
        self.labels = []
        self.str_points = []
        self.end_points = []

    def create_adj_matrix(self) -> None:
        self.adj_matrix = np.zeros((len(self.nodes),len(self.nodes)), dtype=int)
        for line in self.lines:
            info = line.splitlines()[0].split(' ')
            self.adj_matrix[self.nodes.index(info[0])][self.nodes.index(info[2])] = 1
        print(self.adj_matrix)    

    def handle_input(self, input_file: TextIOWrapper) -> None:
        self.nodes = get_line(input_file)
        self.labels = get_line(input_file)
        self.str_points = get_line(input_file)
        self.end_points = get_line(input_file)
        self.lines = input_file.readlines()
        input_file.close()
        self.create_adj_matrix()

    # Szelessegi bejaras
    def BFS(self, s: Any, rev: bool = False) -> None:
        find = -1 if rev else 1
        
        queue = []
        queue.append(s)
        pos = self.nodes.index(s)
        self.visited[pos] = 1

        while queue:
            s = (queue.pop(0))
            pos = self.nodes.index(f'{s}')
            for i in range(len(self.adj_matrix[pos])):
                next_node = self.nodes[i]
                if self.visited[i] == 0:
                    if rev == False:
                        if self.adj_matrix[pos][i] == 1:
                            queue.append(next_node)
                            self.visited[i] = 1
                    else:
                        if self.adj_matrix[i][pos] == 1:
                            queue.append(next_node)
                            self.visited[i] = 1
        print(self.visited)


    def remove_relations(self, node: Any) -> None:
        lines_to_remove = []
        self.visited = np.zeros(len(self.nodes), dtype=int)
        for line in self.lines:
            info = line.splitlines()[0].split(' ')
            if node in info:
               lines_to_remove.append(line)   
        for line in lines_to_remove:
            self.lines.remove(line)

    def remove_not_accessible_nodes(self) -> None:
        self.visited = np.zeros(len(self.nodes), dtype=int)
        for str_point in self.str_points:
            self.BFS(str_point)
        nodes_to_remove = []
        for i in range(len(self.visited)):
            node = self.nodes[i]

            if self.visited[i] == 0:
                nodes_to_remove.append(node)

        for node in nodes_to_remove:
            self.nodes.remove(node)
            self.remove_relations(node)
        self.create_adj_matrix()

    def remove_non_productive_nodes(self) -> None:
        self.visited = np.zeros(len(self.nodes), dtype=int)
        for end_point in self.end_points:
            self.BFS(end_point, rev=True)
        nodes_to_remove = []
        for i in range(len(self.visited)):
            node = self.nodes[i]

            if self.visited[i] == 0:
                nodes_to_remove.append(node)

        for node in nodes_to_remove:
            self.nodes.remove(node)
            self.remove_relations(node)
        self.create_adj_matrix()

    def remove_not_used_labels(self) -> None:
        temp = '\t'.join(self.lines)
        labels_to_remove = []
        for label in self.labels:
            if label not in temp:
                labels_to_remove.append(label)

        for label in labels_to_remove:
            self.labels.remove(label)

    # Writes the automata to the given output file
    def write_result(self, output_file) -> None:
        for node in self.nodes[:-1]:
            output_file.write(f"{node} ")
        output_file.write(self.nodes[-1])
        output_file.write('\n')

        for label in self.labels[:-1]:
            output_file.write(f"{label} ")
        output_file.write(self.labels[-1])
        output_file.write('\n')

        for str_node in self.str_points[:-1]:
            output_file.write(f"{str_node} ")
        output_file.write(self.str_points[-1])
        output_file.write('\n')

        for end_node in self.end_points[:-1]:
            output_file.write(f"{end_node} ")
        output_file.write(self.end_points[-1])
        output_file.write('\n')

        for line in self.lines:
            info = line.splitlines()[0].split(' ')
            output_file.write(f"{info[0]} {info[1]} {info[2]}\n")

if __name__ == "__main__":
    folder_path = pathlib.Path(__file__).parent.resolve()
    input_file = open(f"{folder_path}\input1.in","r")
    output_file = open(f"{folder_path}\output.out","w")

    graph = Graph()
    graph.handle_input(input_file)
    graph.remove_not_accessible_nodes()
    graph.remove_non_productive_nodes()
    graph.remove_not_used_labels()
    graph.write_result(output_file)