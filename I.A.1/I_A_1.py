# Daczo Alpar - daim1803
from io import TextIOWrapper
import pathlib
DOT_code_tmp = """digraph G{
    ranksep =0.5;
    nodesep =0.5;
    rankdir =LR;
    node [ shape ="circle",fontsize ="16" ];
    fontsize ="10";
    compound = true;"""

def get_line(input_file: TextIOWrapper):
    return input_file.readline().splitlines()[0].split(' ')

def create_DOT_code():
    folder_path = pathlib.Path(__file__).parent.resolve()
    input_file = open(f"{folder_path}\\form_I.B.4_b.txt","r")
    output_file = open(f"{folder_path}\output.out","w")
    
    nodes = get_line(input_file)
    labels = get_line(input_file)
    str_points = get_line(input_file)
    end_points = get_line(input_file)
    # Writing customization test into output file
    output_file.write(DOT_code_tmp+"\n\n")

    # Customizing arrows
    for str_point in str_points:
        if str_point not in nodes:
            print(f"Error: {str_point} is not in the nodes")
            output_file.write('The graph is incomplete! Please make sure that the starting points are nodes!')
            exit(1)
        output_file.write(f"\ti{str_point} \t[ shape =point , style = invis ];\n")
    # Customizing ending points
    for end_point in end_points:
        # Validating end_point
        if end_point not in nodes:
            print("Hejj ebbol baj lesz!")
            output_file.write('The graph is incomplete! Please make sure that the ending points are nodes!')
            exit(1)
        
        output_file.write(f"\t{end_point} \t[ shape = doublecircle ];\n")
    
    output_file.write("\n")
    # Arrows for starting points
    print(str_points)
    print(nodes)
    for str_point in str_points:
        output_file.write(f"\ti{str_point} \t-> {str_point};\n")

    lines = input_file.readlines()
    input_file.close()

    for line in lines:
        info = line.splitlines()[0].split(' ')
        if info[0] not in nodes or info[2] not in nodes:
            output_file.write('The graph is incomplete!\nPlease make sure that the nodes are given at the start!')
            exit(1)
        
        if info[1] not in labels:
            output_file.write('The graph is incomplete! Please make sure that the labels are given at the start!')
            exit(1)
        output_file.write(f"\t{info[0]} \t-> {info[2]} [ label = {info[1]} ];\n")
    
    # This is the end
    output_file.write('}')

if __name__ == "__main__":
    create_DOT_code()