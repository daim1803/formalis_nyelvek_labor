# Daczo Alpar - daim1803
from cProfile import label
from io import TextIOWrapper
import pathlib

def get_line(input_file: TextIOWrapper):
    return input_file.readline().splitlines()[0].split(' ')

def return_a_letter(nodes) -> chr:
    alphabet = 'ABCDEFGHIJKLMNOPQRSTVWXY'
    for char in alphabet[::-1]:
        if char not in nodes:
            return char

def reg_to_aut():
    folder_path = pathlib.Path(__file__).parent.resolve()
    input_file = open(f"{folder_path}\input1.in","r")
    output_file = open(f"{folder_path}\output.out","w")

    # Handle input data
    nodes = get_line(input_file)
    plus_statement = 'Z' if 'Z' not in nodes else return_a_letter(nodes)
    nodes.append(plus_statement)
    for node in nodes[:-1]:
        output_file.write(f"{node} ")
    output_file.write(nodes[-1])
    output_file.write("\n")

    labels = get_line(input_file)
    for label in labels[:-1]:
        output_file.write(f"{label} ")
    output_file.write(labels[-1])
    output_file.write("\n")

    str_point = get_line(input_file)
    output_file.write(f"S\n{plus_statement}\n")


    lines = input_file.readlines()
    input_file.close()

    for line in lines:
        info = line.splitlines()[0].split(' ')
        if len(info) == 3:
            output_file.write(f"{info[0]} {info[1]} {info[2]}\n")
        else:
            output_file.write(f"{info[0]} {info[1]} {plus_statement}\n")

if __name__ == "__main__":
    reg_to_aut()