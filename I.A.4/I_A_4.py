import numpy as np
import pathlib
from io import TextIOWrapper

def get_line(input_file: TextIOWrapper) -> list[str]:
    return input_file.readline().splitlines()[0].split(' ')

class Graph:

    def __init__(self) -> None:
        self.nodes = []
        self.labels = []
        self.str_points = []
        self.end_points = []

    def create_adj_matrix(self) -> None:
            self.adj_matrix = np.zeros((len(self.labels),len(self.nodes)), dtype=int)
            for line in self.lines:
                info = line.splitlines()[0].split(' ')
                self.adj_matrix[self.labels.index(info[1])][self.nodes.index(info[0])] = info[2]

    def handle_input(self, input_file: TextIOWrapper) -> None:
        self.nodes = get_line(input_file)
        self.labels = get_line(input_file)
        self.str_points = get_line(input_file)
        self.end_points = get_line(input_file)
        self.lines = input_file.readlines()
        input_file.close()
        self.create_adj_matrix()

    def check_for_word(self, word: str) -> bool:
        for it in range(len(word)):
            char = word[it]
            char_ind = self.labels.index(char)
            print(char_ind)
            print(char)
            if it == 0:
                print("first_blood")
            
        return True

if __name__ == "__main__":
    folder_path = pathlib.Path(__file__).parent.resolve()
    input_file = open(f"{folder_path}\input1.in","r")
    output_file = open(f"{folder_path}\output.out","w")
    graph = Graph()
    graph.handle_input(input_file)
    print(graph.adj_matrix)
    while True:
        word = input('Enter a word: ')
        if word == '':
            break
        graph.check_for_word(word)
        

    print('Bye')