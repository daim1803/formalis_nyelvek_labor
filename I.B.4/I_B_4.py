# Daczo Alpar - daim1803
from io import TextIOWrapper
from typing import Any
import numpy as np
import pathlib

def get_line(input_file: TextIOWrapper) -> list[str]:
    return input_file.readline().splitlines()[0].split(' ')
    
class DFAGraph:

    def __init__(self) -> None:
        self.nodes = []
        self.labels = []
        self.str_points = []
        self.end_points = []

    def create_nfa_transition_table(self) -> None:
        self.nfa_trans_table = np.zeros((len(self.nfa_nodes),len(self.labels)), dtype=object)
        for line in self.nfa_lines:
            info = line.splitlines()[0].split(' ')
            if self.nfa_trans_table[self.nfa_nodes.index(info[0])][self.labels.index(info[1])] == 0:
                self.nfa_trans_table[self.nfa_nodes.index(info[0])][self.labels.index(info[1])] = [info[2]]
            else:
                self.nfa_trans_table[self.nfa_nodes.index(info[0])][self.labels.index(info[1])].append(info[2])

    def create_complete_dfa_graph(self) -> None:
        new_node = self.get_new_node()
        self.nodes.append(new_node)
        self.graph = np.append(self.graph, [[new_node] * len(self.labels)], 0)
        for row in range(len(self.nodes)):
            for col in range(len(self.labels)):
                if self.graph[row][col] == '':
                    self.graph[row][col] = new_node

    def handle_input(self, input_file: TextIOWrapper) -> None:
        self.nfa_nodes = get_line(input_file)
        self.labels = get_line(input_file)
        self.nfa_str_points = get_line(input_file)
        self.nfa_end_points = get_line(input_file)
        self.nfa_lines = input_file.readlines()
        input_file.close()
        self.create_nfa_transition_table()
        print(self.nfa_trans_table)

    def convert_nfa_to_dfa(self):
        self.init_state = self.nfa_str_points[0]
        self.final_states = self.nfa_end_points

        
        
        

if __name__ == "__main__":
    folder_path = pathlib.Path(__file__).parent.resolve()
    input_file = open(f"{folder_path}\\form_I.B.4_b.txt","r")
    output_file = open(f"{folder_path}\output.out","w")

    graph1 = DFAGraph()
    graph1.handle_input(input_file)
    graph1.convert_nfa_to_dfa()