# Daczo Alpar - daim1803
from io import TextIOWrapper
from typing import Any
import numpy as np
import pathlib

def get_line(input_file: TextIOWrapper) -> list[str]:
    return input_file.readline().splitlines()[0].split(' ')

class Graph:

    def __init__(self) -> None:
        self.nodes = []
        self.labels = []
        self.str_points = []
        self.end_points = []

    def create_adj_matrix(self) -> None:
        self.adj_matrix = np.zeros((len(self.nodes),len(self.nodes)), dtype=int)
        for line in self.lines:
            info = line.splitlines()[0].split(' ')
            self.adj_matrix[self.nodes.index(info[0])][self.nodes.index(info[2])] = 1

    def create_dfa_graph(self) -> None:
        self.graph = np.zeros((len(self.nodes),len(self.labels)), dtype=str)
        for line in self.lines:
            info = line.splitlines()[0].split(' ')
            self.graph[self.nodes.index(info[0])][self.labels.index(info[1])] = info[2]

    def create_complete_dfa_graph(self) -> None:
        new_node = self.get_new_node()
        self.nodes.append(new_node)
        self.graph = np.append(self.graph, [[new_node] * len(self.labels)], 0)
        for row in range(len(self.nodes)):
            for col in range(len(self.labels)):
                if self.graph[row][col] == '':
                    self.graph[row][col] = new_node

    def handle_input(self, input_file: TextIOWrapper) -> None:
        self.nodes = get_line(input_file)
        self.labels = get_line(input_file)
        self.str_points = get_line(input_file)
        self.end_points = get_line(input_file)
        self.lines = input_file.readlines()
        input_file.close()
        self.create_adj_matrix()

    def get_new_node(self):
        numbers = '0123456789'
        alphabet_upper = 'ABCDEFGHIJKLMNOPQRSTVWXYZ'
        alphabet_lower = 'abcdefghijklmnopqrstvwxyz'
        
        node = self.nodes[0]
        if node in numbers:
            for num in numbers[::-1]:
                if num not in self.nodes:
                    return num

        if node in alphabet_lower:
            for char in alphabet_lower[::-1]:
                if char not in self.nodes:
                    return char

        if node in alphabet_upper:
            for char in alphabet_upper[::-1]:
                if char not in self.nodes:
                    return char

    def check_equivalence(self, other_graph) -> bool:
        queue = []
        visited = []
        current_nodes = (self.str_points[0],other_graph.str_points[0])
        queue.append(current_nodes)

        while queue:
            current_nodes = queue.pop(0)
            visited.append(current_nodes)

            if current_nodes[0] in self.end_points and current_nodes[1] not in other_graph.end_points \
                or current_nodes[0] not in self.end_points and current_nodes[1] in other_graph.end_points:
                return False

            length = min(len(self.labels), len(other_graph.labels))
            print(f"{current_nodes} - ",end="")
            for label_pos in range(length):
                next_node = (self.graph[self.nodes.index(current_nodes[0])][label_pos],
                            other_graph.graph[other_graph.nodes.index(current_nodes[1])][label_pos])
                print(f"{next_node} ",end="")
                if next_node not in visited:
                    queue.append(next_node)
            print()                
        return True

    # Szelessegi bejaras
    def BFS(self, s: Any, rev: bool = False) -> None:
        find = -1 if rev else 1
        
        queue = []
        queue.append(s)
        pos = self.nodes.index(s)
        self.visited[pos] = 1

        while queue:
            s = (queue.pop(0))
            pos = self.nodes.index(f'{s}')
            for i in range(len(self.adj_matrix[pos])):
                next_node = self.nodes[i]
                if self.visited[i] == 0:
                    if rev == False:
                        if self.adj_matrix[pos][i] == 1:
                            queue.append(next_node)
                            self.visited[i] = 1
                    else:
                        if self.adj_matrix[i][pos] == 1:
                            queue.append(next_node)
                            self.visited[i] = 1

    def remove_relations(self, node: Any) -> None:
        lines_to_remove = []
        self.visited = np.zeros(len(self.nodes), dtype=int)
        for line in self.lines:
            info = line.splitlines()[0].split(' ')
            if node in info:
               lines_to_remove.append(line)   
        for line in lines_to_remove:
            self.lines.remove(line)

    def remove_not_accessible_nodes(self) -> None:
        self.visited = np.zeros(len(self.nodes), dtype=int)
        for str_point in self.str_points:
            self.BFS(str_point)
        nodes_to_remove = []
        for i in range(len(self.visited)):
            node = self.nodes[i]

            if self.visited[i] == 0:
                nodes_to_remove.append(node)

        for node in nodes_to_remove:
            self.nodes.remove(node)
            self.remove_relations(node)
        self.create_adj_matrix()

    def remove_non_productive_nodes(self) -> None:
        self.visited = np.zeros(len(self.nodes), dtype=int)
        for end_point in self.end_points:
            self.BFS(end_point, rev=True)
        nodes_to_remove = []
        for i in range(len(self.visited)):
            node = self.nodes[i]

            if self.visited[i] == 0:
                nodes_to_remove.append(node)

        for node in nodes_to_remove:
            self.nodes.remove(node)
            self.remove_relations(node)
        self.create_adj_matrix()

    def remove_not_used_labels(self) -> None:
        temp = '\t'.join(self.lines)
        labels_to_remove = []
        for label in self.labels:
            if label not in temp:
                labels_to_remove.append(label)

        for label in labels_to_remove:
            self.labels.remove(label)

    # Writes the automata to the given output file
    def write_result(self, output_file) -> None:
        for node in self.nodes[:-1]:
            output_file.write(f"{node} ")
        output_file.write(self.nodes[-1])
        output_file.write('\n')

        for label in self.labels[:-1]:
            output_file.write(f"{label} ")
        output_file.write(self.labels[-1])
        output_file.write('\n')

        for str_node in self.str_points[:-1]:
            output_file.write(f"{str_node} ")
        output_file.write(self.str_points[-1])
        output_file.write('\n')

        for end_node in self.end_points[:-1]:
            output_file.write(f"{end_node} ")
        output_file.write(self.end_points[-1])
        output_file.write('\n')

        for line in self.lines:
            info = line.splitlines()[0].split(' ')
            output_file.write(f"{info[0]} {info[1]} {info[2]}\n")


    def check_states(self, state1: str, state2: str) -> bool:
        for label in range(len(self.labels)):
            if self.graph[self.nodes.index(state1)][label] in self.end_points and self.graph[self.nodes.index(state2)][label] not in self.end_points\
                or self.graph[self.nodes.index(state1)][label] not in self.end_points and self.graph[self.nodes.index(state2)][label] in self.end_points:
                return False
        return True

    def minimization_of_dfa(self):
        num_of_states = len(self.nodes)
        myhill_nerode_table = [[0] * num_of_states for i in range(num_of_states)]
        # Mark final states
        for i in range(num_of_states):
            for j in range(num_of_states):
                if not self.check_states(self.nodes[i],self.nodes[j]) and j < i:
                    myhill_nerode_table[i][j] = '*'

        for i in range(num_of_states):
            for j in range(num_of_states):
                if myhill_nerode_table[i][j] == 0 and j < i:
                    myhill_nerode_table[i][j] = []

        for i in range(num_of_states):
            for j in range(num_of_states):
                if myhill_nerode_table[i][j] == [] and j < i:
                    for label_num in range(len(self.labels)):
                        p = self.graph[i][label_num]
                        q = self.graph[j][label_num]
                        print(f"{p} ---- {q}")
                        p_index = self.nodes.index(p)
                        q_index = self.nodes.index(q)
                        if myhill_nerode_table[p_index][q_index] == '*':
                            myhill_nerode_table[i][j] = '*'
                    print('----------------------')
        self.print_table(myhill_nerode_table)
        
    def print_table(self, table):
        print(self.nodes)
        for row in table:
            print(row)
        print()

if __name__ == "__main__":
    folder_path = pathlib.Path(__file__).parent.resolve()
    input_file1 = open(f"{folder_path}\\form_I.B.3.txt","r")
    output_file = open(f"{folder_path}\output.out","w")

    graph1 = Graph()
    graph1.handle_input(input_file1)
    graph1.remove_not_accessible_nodes()
    graph1.create_dfa_graph()
    print(graph1.graph)
    print('-----------------------')
    #graph1.create_complete_dfa_graph()
    graph1.minimization_of_dfa()
